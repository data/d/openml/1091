# OpenML dataset: SMSA

https://www.openml.org/d/1091

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/SMSA.html

Air Pollution and Mortality

Reference:   U.S. Department of Labor Statistics
Authorization:   free use
Description:   Properties of 60 Standard Metropolitan Statistical Areas (a standard Census Bureau designation of the region around a city) in the United States, collected from a variety of sources.
The data include information on the social and economic conditions in these areas, on their climate, and some indices of air pollution potentials.
Number of cases:   60
Variable Names:

city:   City name
JanTemp:   Mean January temperature (degrees Farenheit)
JulyTemp:   Mean July temperature (degrees Farenheit)
RelHum:   Relative Humidity
Rain:   Annual rainfall (inches)
Mortality:   Age adjusted mortality
Education:   Median education
PopDensity:   Population density
%NonWhite:   Percentage of non whites
%WC:   Percentage of white collar workers
pop:   Population
pop/house:   Population per household
income:   Median income
HCPot:   HC pollution potential
NOxPot:   Nitrous Oxide pollution potential
SO2Pot:   Sulfur Dioxide pollution potential
NOx:   Nitrous Oxide

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1091) of an [OpenML dataset](https://www.openml.org/d/1091). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1091/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1091/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1091/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

